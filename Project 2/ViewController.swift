//
//  ViewController.swift
//  Project 2
//
//  Created by Jo on 4/4/19.
//  Copyright © 2019 Aleksandr Duma. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var firstFlag: UIButton!
    @IBOutlet var secondFlag: UIButton!
    @IBOutlet var thirdFlag: UIButton!
    
    var countries = [String]()
    var score = 0
    var correctAnswer = 0
    var gamesCount = 0
    let numberOfGames = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countries += ["estonia", "france", "germany", "ireland", "nigeria", "poland", "russia", "spain", "uk", "us"]
        
        setButtonsVisuals()
        askQuestion()
        
    }
    
    func askQuestion(action: UIAlertAction! = nil){
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
        
        firstFlag.setImage(UIImage(named: countries[0]), for: .normal)
        secondFlag.setImage(UIImage(named: countries[1]), for: .normal)
        thirdFlag.setImage(UIImage(named: countries[2]), for: .normal)
        
        title = "Chose "+countries[correctAnswer].uppercased()+". Current score: \(score)"
    }
    
    func setButtonsVisuals(){
        firstFlag.layer.borderWidth = 1
        secondFlag.layer.borderWidth = 1
        thirdFlag.layer.borderWidth = 1
        
        firstFlag.layer.borderColor = UIColor.lightGray.cgColor
        secondFlag.layer.borderColor = UIColor.lightGray.cgColor
        thirdFlag.layer.borderColor = UIColor.lightGray.cgColor
    }
    @IBAction func buttonTapped(_ sender: UIButton) {
        var title: String
        
        gamesCount += 1
        
        if sender.tag == correctAnswer{
            title = "Correct"
            score += 1
        }else{
            title = "Wrong. It`s \(countries[sender.tag].uppercased())"
            score -= 1
        }
        if gamesCount < numberOfGames{
            let ac = UIAlertController(title: title, message: "Your score is \(score)", preferredStyle: .alert)
            
            ac.addAction(UIAlertAction(title: "Continue", style: .default, handler: askQuestion))
            
            present(ac, animated: true)
        }else{
            let ac = UIAlertController(title: "Final score is \(score)", message: "Do you want to restart?", preferredStyle: .alert)
            
            ac.addAction(UIAlertAction(title: "Restart", style: .default, handler: askQuestion))
            
            score = 0
            gamesCount = 0
            
            present(ac, animated: true)
        }
    }
    
}

